import { WorkoutsPage } from '../workouts/workouts';
import { WorkoutService } from '../../app/services/workout.service';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the WorkoutDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-workout-details',
  templateUrl: 'workout-details.html',
})
export class WorkoutDetailsPage {
  public workout: any;
  public result: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private workoutService: WorkoutService) {
    this.workout = navParams.get('workout')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WorkoutDetailsPage');
  }
  delete(id){
    this.workoutService.delete(id).subscribe(data => this.result = data);
    this.navCtrl.push(WorkoutsPage);
  }

}
