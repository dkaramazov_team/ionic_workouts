import { WorkoutsPage } from '../workouts/workouts';
import { WorkoutService } from '../../app/services/workout.service';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AddWorkoutPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-workout',
  templateUrl: 'add-workout.html',
})
export class AddWorkoutPage {
  public title:string;
  public note: string;
  public type: string;
  public result: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private workoutService: WorkoutService) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddWorkoutPage');
  }
  onSubmit(){
    let workout = {
      title: this.title,
      note: this.note,
      type: this.type
    }
    this.workoutService.addWorkout(workout).subscribe(data => this.result = data);
    this.navCtrl.push(WorkoutsPage);
  }
}
