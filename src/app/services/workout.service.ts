import {Injectable} from '@angular/core';
import {Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class WorkoutService{
    http: any;
    apiKey: string;
    workoutsUrl: string;
    private headers: Headers = new Headers();

    constructor(http: Http){
        this.http = http;
        this.apiKey = 'vVH4pKyzeY67OG2oEjjsZW0uTmFYvzNR';
        this.workoutsUrl = 'https://api.mlab.com/api/1/databases/ionic_workouts/collections/workouts';
        // this.workoutsUrl = 'https://dkaramazov:realpassword@ds127163.mlab.com:27163/ionic_workouts';

        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Access-Control-Allow-Origin', `*`);
        this.headers.append('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
        this.headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token');
    }

    getWorkouts(){
        return this.http.get(`${this.workoutsUrl}?apiKey=${this.apiKey}`, {headers: this.headers})
            .map(res => res.json());
    }

    addWorkout(workout){
        return this.http.post(`${this.workoutsUrl}?apiKey=${this.apiKey}`, JSON.stringify(workout), {headers: this.headers})
            .map(res => res.json());
    }

    delete(id){
        return this.http.delete(`${this.workoutsUrl}/${id}?apiKey=${this.apiKey}`, JSON.stringify(id), {headers: this.headers})
            .map(res => res.json());
    }
}